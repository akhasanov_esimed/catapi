package services

import model.Breed
import model.Image
import kotlin.js.Json

class CatService {
    val api_key ="a7471e3a-f530-4eed-8ca6-7beaac7173b9"
    val api_name = "x-api-key"
    fun getBreeds(callback: (Array<Breed>)->Unit){
        val url_breeds = "https://api.thecatapi.com/v1/breeds"
        Tools.getAsync(url_breeds,{it.setRequestHeader(api_name,api_key)},{callback(JSON.parse(it))})
    }

    fun getImages(race_id:String,callback: (Array<Image>) -> Unit){
        var url_images = "https://api.thecatapi.com/v1/images/search?breed_ids=$race_id&limit=10"
        Tools.getAsync(url_images,{it.setRequestHeader(api_name,api_key)},{callback(JSON.parse(it))})
    }
}