package controllers

import org.w3c.dom.*
import org.w3c.dom.events.MouseEvent
import services.CatService
import kotlin.browser.document
import kotlin.dom.createElement

class IndexController {
    val catService = CatService()
    val HTMLSelectElement = document.getElementById("select_element") as HTMLSelectElement
    val HTMLImageDiv= document.getElementById("image-div")
    init {
       this.catService.getBreeds {
           it.forEach {
              val option = document.createElement("option") as HTMLOptionElement
               option.value = it.id.toString()
               option.innerHTML = it.name
               HTMLSelectElement.appendChild(option)

           }
       }
        HTMLSelectElement.onclick = {
            changeBreed(it)
        }

    }

    val changeBreed = fun (e:MouseEvent):dynamic {
        console.log("change breed")
        val race_id = HTMLSelectElement.value
        println(race_id)
        if(race_id.isNotEmpty()) {
            HTMLImageDiv?.innerHTML=""
            catService.getImages(race_id, {
                it.forEach {
                    val divRow = document.createElement("div") as HTMLDivElement
                    divRow.className="image"
                    divRow.align="center"
                    val image = document.createElement("img") as HTMLImageElement
                    image.src = it.url
                    image.className = "img-thumbnail"
                    divRow.append(image)
                    HTMLImageDiv?.append(divRow)
                }
            })
        }
        return true
    }



    val HTMLElement = document.getElementById("loading_spinner")
}