import org.w3c.xhr.XMLHttpRequest

object Tools {

    fun getAsync(
        url: String,
        prepare: (XMLHttpRequest) -> Unit,
        callback: (String) -> Unit
    ) {
        val xmlHttp = XMLHttpRequest()
        xmlHttp.open("GET", url)
        prepare(xmlHttp)
        xmlHttp.onload = {
            if (xmlHttp.readyState == 4.toShort() && xmlHttp.status == 200.toShort()) {
                callback(xmlHttp.responseText)
            }
        }
        xmlHttp.send()
    }

}
