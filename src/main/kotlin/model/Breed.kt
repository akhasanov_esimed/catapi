package model

data class Breed (val id:Int, val name:String,val origin:String)