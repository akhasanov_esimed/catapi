define(['exports', 'kotlin'], function (_, Kotlin) {
  'use strict';
  var throwCCE = Kotlin.throwCCE;
  var Unit = Kotlin.kotlin.Unit;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var toShort = Kotlin.toShort;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  function IndexController() {
    this.catService = new CatService();
    var tmp$;
    this.HTMLSelectElement = Kotlin.isType(tmp$ = document.getElementById('select_element'), HTMLSelectElement) ? tmp$ : throwCCE();
    this.HTMLImageDiv = document.getElementById('image-div');
    this.catService.getBreeds_m91xn2$(IndexController_init$lambda(this));
    this.HTMLSelectElement.onclick = IndexController_init$lambda_0(this);
    this.changeBreed = IndexController$changeBreed$lambda(this);
    this.HTMLElement = document.getElementById('loading_spinner');
  }
  function IndexController_init$lambda(this$IndexController) {
    return function (it) {
      var tmp$;
      for (tmp$ = 0; tmp$ !== it.length; ++tmp$) {
        var element = it[tmp$];
        var this$IndexController_0 = this$IndexController;
        var tmp$_0;
        var option = Kotlin.isType(tmp$_0 = document.createElement('option'), HTMLOptionElement) ? tmp$_0 : throwCCE();
        option.value = element.id.toString();
        option.innerHTML = element.name;
        this$IndexController_0.HTMLSelectElement.appendChild(option);
      }
      return Unit;
    };
  }
  function IndexController_init$lambda_0(this$IndexController) {
    return function (it) {
      return this$IndexController.changeBreed(it);
    };
  }
  function IndexController$changeBreed$lambda$lambda(this$IndexController) {
    return function (it) {
      var tmp$;
      for (tmp$ = 0; tmp$ !== it.length; ++tmp$) {
        var element = it[tmp$];
        var this$IndexController_0 = this$IndexController;
        var tmp$_0, tmp$_1, tmp$_2;
        var divRow = Kotlin.isType(tmp$_0 = document.createElement('div'), HTMLDivElement) ? tmp$_0 : throwCCE();
        divRow.className = 'image';
        divRow.align = 'center';
        var image = Kotlin.isType(tmp$_1 = document.createElement('img'), HTMLImageElement) ? tmp$_1 : throwCCE();
        image.src = element.url;
        image.className = 'img-thumbnail';
        divRow.append(image);
        (tmp$_2 = this$IndexController_0.HTMLImageDiv) != null ? (tmp$_2.append(divRow), Unit) : null;
      }
      return Unit;
    };
  }
  function IndexController$changeBreed$lambda(this$IndexController) {
    return function (e) {
      var tmp$;
      console.log('change breed');
      var race_id = this$IndexController.HTMLSelectElement.value;
      println(race_id);
      if (race_id.length > 0) {
        (tmp$ = this$IndexController.HTMLImageDiv) != null ? (tmp$.innerHTML = '') : null;
        this$IndexController.catService.getImages_82d4yh$(race_id, IndexController$changeBreed$lambda$lambda(this$IndexController));
      }
      return true;
    };
  }
  IndexController.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'IndexController',
    interfaces: []
  };
  function main() {
    println('hello');
    var cat = new CatService();
    new IndexController();
  }
  function Breed(id, name, origin) {
    this.id = id;
    this.name = name;
    this.origin = origin;
  }
  Breed.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Breed',
    interfaces: []
  };
  Breed.prototype.component1 = function () {
    return this.id;
  };
  Breed.prototype.component2 = function () {
    return this.name;
  };
  Breed.prototype.component3 = function () {
    return this.origin;
  };
  Breed.prototype.copy_s4fhmi$ = function (id, name, origin) {
    return new Breed(id === void 0 ? this.id : id, name === void 0 ? this.name : name, origin === void 0 ? this.origin : origin);
  };
  Breed.prototype.toString = function () {
    return 'Breed(id=' + Kotlin.toString(this.id) + (', name=' + Kotlin.toString(this.name)) + (', origin=' + Kotlin.toString(this.origin)) + ')';
  };
  Breed.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    result = result * 31 + Kotlin.hashCode(this.origin) | 0;
    return result;
  };
  Breed.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.name, other.name) && Kotlin.equals(this.origin, other.origin)))));
  };
  function Image(url) {
    this.url = url;
  }
  Image.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Image',
    interfaces: []
  };
  Image.prototype.component1 = function () {
    return this.url;
  };
  Image.prototype.copy_61zpoe$ = function (url) {
    return new Image(url === void 0 ? this.url : url);
  };
  Image.prototype.toString = function () {
    return 'Image(url=' + Kotlin.toString(this.url) + ')';
  };
  Image.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.url) | 0;
    return result;
  };
  Image.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && Kotlin.equals(this.url, other.url))));
  };
  function CatService() {
    this.api_key = 'a7471e3a-f530-4eed-8ca6-7beaac7173b9';
    this.api_name = 'x-api-key';
  }
  function CatService$getBreeds$lambda(this$CatService) {
    return function (it) {
      it.setRequestHeader(this$CatService.api_name, this$CatService.api_key);
      return Unit;
    };
  }
  function CatService$getBreeds$lambda_0(closure$callback) {
    return function (it) {
      closure$callback(JSON.parse(it));
      return Unit;
    };
  }
  CatService.prototype.getBreeds_m91xn2$ = function (callback) {
    var url_breeds = 'https://api.thecatapi.com/v1/breeds';
    Tools_getInstance().getAsync_31w93v$(url_breeds, CatService$getBreeds$lambda(this), CatService$getBreeds$lambda_0(callback));
  };
  function CatService$getImages$lambda(this$CatService) {
    return function (it) {
      it.setRequestHeader(this$CatService.api_name, this$CatService.api_key);
      return Unit;
    };
  }
  function CatService$getImages$lambda_0(closure$callback) {
    return function (it) {
      closure$callback(JSON.parse(it));
      return Unit;
    };
  }
  CatService.prototype.getImages_82d4yh$ = function (race_id, callback) {
    var url_images = 'https://api.thecatapi.com/v1/images/search?breed_ids=' + race_id + '&limit=10';
    Tools_getInstance().getAsync_31w93v$(url_images, CatService$getImages$lambda(this), CatService$getImages$lambda_0(callback));
  };
  CatService.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CatService',
    interfaces: []
  };
  function Tools() {
    Tools_instance = this;
  }
  function Tools$getAsync$lambda(closure$xmlHttp, closure$callback) {
    return function (it) {
      if (closure$xmlHttp.readyState === toShort(4) && closure$xmlHttp.status === toShort(200)) {
        closure$callback(closure$xmlHttp.responseText);
      }
      return Unit;
    };
  }
  Tools.prototype.getAsync_31w93v$ = function (url, prepare, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET', url);
    prepare(xmlHttp);
    xmlHttp.onload = Tools$getAsync$lambda(xmlHttp, callback);
    xmlHttp.send();
  };
  Tools.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Tools',
    interfaces: []
  };
  var Tools_instance = null;
  function Tools_getInstance() {
    if (Tools_instance === null) {
      new Tools();
    }
    return Tools_instance;
  }
  var package$controllers = _.controllers || (_.controllers = {});
  package$controllers.IndexController = IndexController;
  _.main = main;
  var package$model = _.model || (_.model = {});
  package$model.Breed = Breed;
  package$model.Image = Image;
  var package$services = _.services || (_.services = {});
  package$services.CatService = CatService;
  Object.defineProperty(_, 'Tools', {
    get: Tools_getInstance
  });
  main();
  Kotlin.defineModule('app', _);
  return _;
});

//# sourceMappingURL=app.js.map
